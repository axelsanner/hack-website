from flask import Flask

VERSION = (0, 0, 1)


def get_version():
    """
    A silly version getter.
    """
    return "%d.%d.%d" % VERSION


def create_app():
    args = {}
    args['instance_path'] = '/home/axelbsa/src/hound/hound'
    app = Flask(__name__, **args)
    app.config.from_pyfile('conf/config.py')
    app.config.update(SECRET_KEY='development key')

    from root_pages import init_pages
    init_pages(app)

    return app
