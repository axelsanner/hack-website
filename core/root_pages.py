import pymongo as mongo
import bcrypt
import os

from operator import itemgetter

from werkzeug import secure_filename
from flask import (Flask, render_template, flash, redirect, session,
                   url_for, request, Blueprint, request, jsonify, json)

args = {}
app = Flask(__name__, **args)
app.config.from_pyfile('conf/config.py')
app.config.update(SECRET_KEY='development key')

UPLOAD_FOLDER = '/tmp/programming'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'raw'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def init_pages(app):
    @app.route('/')
    def index():
        if not session.get('login'):
            return redirect(url_for('login'))
        return redirect(url_for('render_main'))

    @app.route('/login', methods=['GET', 'POST'])
    def login():
        error = None
        conn = mongo.MongoClient('localhost', w=1).hackaton
        if request.method == 'POST':
            username = request.form['username']

            if username.strip() == '':
                error = "wrong username asss"
                return render_template('login.html', error=error)
            password = request.form['password']
            password = password.encode('utf-8')
            try:
                result = conn.users.find(
                    {"email": username}, {"password_bc": 1}
                )
                pw_hash = result[0]['password_bc']
                pw_hash = pw_hash.encode('utf-8')
                print "found user"
                if bcrypt.hashpw(password, pw_hash) == pw_hash:
                    session['login'] = True
                    session['username'] = request.form['username']
                    flash('You are logged in')
                    return redirect(url_for('render_main'))
                else:
                    print "Wrong hash"
            except Exception as e:
                result = conn.users.find_one({"email": username})
                if not result:
                    categories = {
                        "web": {},
                        "crypto": {},
                        "afk": {},
                        "reveng": {}
                    }
                    password_bc = bcrypt.hashpw(password, bcrypt.gensalt())
                    conn.users.insert({
                        "password_bc": password_bc,
                        "email": username,
                        "ch": categories,
                        "score": 0,
                        "user_id": username
                    })
                    session['login'] = True
                    session['username'] = request.form['username']
                    flash('You are logged in')
                    return redirect(url_for('render_main'))

                print "No such user or passord is wrong: " + str(e)
                error = "Wrong password or something..."

        return render_template('login.html', error=error)

    @app.route('/logout', methods=['GET'])
    def logout():
        session.pop('login', None)
        flash('You were logged out')
        return redirect(url_for('login'))

    @app.route('/main', methods=['GET'])
    def render_main():
        if not session.get('login'):
            return redirect(url_for('login'))

        conn = mongo.MongoClient('localhost', w=1).hackaton
        standings = list(conn.users.find({}, {"email": 1,
                                              "score": 1, u"_id": 0}))
        result = sorted(standings, key=itemgetter('score'), reverse=True)
        print result
        return render_template('main.html', entries=result,
                               username=session['username'])

    @app.route('/afk', methods=['GET', 'POST'])
    def render_afk():
        if request.method == 'POST':
            c_type = 'afk'
            token = request.form['token']
            challenge_id = int(request.form['challenge'])
            conn = mongo.MongoClient('localhost', w=1).hackaton

            query = {"challenge_id": challenge_id, "challenge_type": c_type}
            proj = {"challenge_token": 1, "challenge_score": 1}
            result = conn.challenges.find_one(query, proj)
            if result and token == result['challenge_token']:
                user = session['username']
                score = conn.users.find_one({"email": user},
                                            {"score": 1, "user_id": 1,
                                             "ch.afk": 1})
                if str(challenge_id) not in score['ch']['afk']:
                    print "You have not solved this"
                    new_score = score['score'] + result['challenge_score']
                    ph = "ch.afk.%s" % challenge_id
                    conn.users.update({"user_id": score["user_id"]},
                                      {"$set": {"score": int(new_score),
                                                ph: 1}})
                    print new_score
            else:
                print "sadness"
            # Do the lookup and assign a score if right

        return render_template('afk_main.html',
                               username=session['username'])

    @app.route('/crypto', methods=['GET', 'POST'])
    def render_crypto():
        if request.method == 'POST':
            c_type = 'crypto'
            token = request.form['token']
            challenge_id = int(request.form['challenge'])
            conn = mongo.MongoClient('localhost', w=1).hackaton

            query = {"challenge_id": challenge_id, "challenge_type": c_type}
            proj = {"challenge_token": 1, "challenge_score": 1}
            result = conn.challenges.find_one(query, proj)
            if result and token == result['challenge_token']:
                user = session['username']
                score = conn.users.find_one({"email": user},
                                            {"score": 1, "user_id": 1,
                                             "ch.crypto": 1})
                if (str(challenge_id) not in score['ch']['crypto']):
                    print "You have not solved this"
                    new_score = score['score'] + result['challenge_score']
                    ph = "ch.crypto.%s" % challenge_id
                    conn.users.update({"user_id": score["user_id"]},
                                      {"$set": {"score": int(new_score),
                                                ph: 1}})
                    print new_score
            else:
                print "sadness"
            # Do the lookup and assign a score if right

        return render_template('crypto_main.html',
                               username=session['username'])
        # Just render the shit

    @app.route('/web', methods=['GET', 'POST'])
    def render_web():
        if request.method == 'POST':
            c_type = 'web'
            token = request.form['token']
            challenge_id = int(request.form['challenge'])
            print challenge_id
            conn = mongo.MongoClient('localhost', w=1).hackaton

            query = {"challenge_id": challenge_id, "challenge_type": c_type}
            proj = {"challenge_token": 1, "challenge_score": 1}
            result = conn.challenges.find_one(query, proj)
            print result
            if result and token == result['challenge_token']:
                user = session['username']
                score = conn.users.find_one({"email": user},
                                            {"score": 1, "user_id": 1,
                                             "ch.web": 1})
                if str(challenge_id) not in score['ch']['web']:
                    print "You have not solved this"
                    ph = "ch.web.%s" % challenge_id
                    new_score = score['score'] + result['challenge_score']
                    conn.users.update({"user_id": score["user_id"]},
                                      {"$set": {"score": int(new_score),
                                                ph: 1}})
                    print new_score
            else:
                print "sadness"
            # Do the lookup and assign a score if right

        return render_template('web_main.html', username=session['username'])
        # Just render the shit

    @app.route('/reveng', methods=['GET', 'POST'])
    def render_reverse():
        if request.method == 'POST':
            c_type = 'reveng'
            token = request.form['token']
            challenge_id = int(request.form['challenge'])
            print challenge_id
            conn = mongo.MongoClient('localhost', w=1).hackaton

            query = {"challenge_id": challenge_id, "challenge_type": c_type}
            proj = {"challenge_token": 1, "challenge_score": 1}
            result = conn.challenges.find_one(query, proj)
            print result
            if result and token == result['challenge_token']:
                user = session['username']
                score = conn.users.find_one({"email": user},
                                            {"score": 1, "user_id": 1,
                                             "ch.reveng": 1})
                if str(challenge_id) not in score['ch']['reveng']:
                    print "You have not solved this"
                    ph = "ch.reveng.%s" % challenge_id
                    new_score = score['score'] + result['challenge_score']
                    conn.users.update({"user_id": score["user_id"]},
                                      {"$set": {"score": int(new_score),
                                                ph: 1}})
                    print new_score
            else:
                print "sadness"
            # Do the lookup and assign a score if right

        return render_template(
            'reverse_main.html', username=session['username']
        )

    def allowed_file(filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    @app.route('/programming', methods=['GET', 'POST'])
    def render_programming():
        if request.method == 'POST':

            ff = request.files['file']
            if ff and allowed_file(ff.filename):
                filename = secure_filename(ff.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                return redirect(url_for('uploaded_file',
                                        filename=filename))
        return render_template(
            'index.html', username=session['username']
        )
        # Just render the shit

    @app.route('/json_render', methods=['GET'])
    def json_test():
        some_static_data = {
            "Type": "Value",
            "A_string": "More_String",
            "a_list": [
                1,
                2,
                4,
                9
            ]
        }
        return jsonify(some_static_data)


init_pages(app)
